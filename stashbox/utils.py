#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hashlib

DROPBOX_HASH_CHUNK_SIZE = 4 * 1024 * 1024


def compute_dropbox_hash(filename):
    """
    Calculating hash for local files to enable comparison with files on Dropbox.

    See:
        https://www.dropbox.com/developers/reference/content-hash
        https://github.com/dropbox/dropbox-api-content-hasher/tree/master/python
        https://stackoverflow.com/questions/13008040/locally-calculate-dropbox-hash-of-files

    Args:
        filename (str): Path to file to compute hash for

    Returns:
        str: Computed file hash

    """
    with open(filename, 'rb') as f:
        block_hashes = b''
        while True:
            chunk = f.read(DROPBOX_HASH_CHUNK_SIZE)
            if not chunk:
                break
            block_hashes += hashlib.sha256(chunk).digest()
        return hashlib.sha256(block_hashes).hexdigest()
