#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

# Dropbox access token
DROPBOX_ACCESS_TOKEN = os.getenv('SB_ACCESS_TOKEN')

# Logging
LOG_DIR = os.getenv('SB_LOGDIR') or '/tmp'

# Local directory to be backed up
SOURCE = os.path.expanduser(os.getenv('SB_SOURCE') or '')

# Dropbox directory to upload files to
DESTINATION = os.getenv('SB_DESTINATION') or '/StashBox'

# Interval between each backup
INTERVAL = float(os.getenv('SB_INTERVAL', '360'))
