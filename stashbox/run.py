#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

from dropbox.exceptions import BadInputError
from stashbox.exceptions import FatalStashBoxError
from stashbox.main import StashBox
from stashbox.logger import setup_logger

setup_logger()
logger = logging.getLogger(__name__)


def run():
    """
    Runs the StashBox application.
    """
    while True:
        try:
            stash_box = StashBox()
            stash_box.start()
        except KeyboardInterrupt:
            logger.info('StashBox terminated by keyboard interrupt.')
            break
        except BadInputError:
            logger.exception('StashBox stopped due to bad input: ')
            break
        except FatalStashBoxError:
            logger.exception('StashBox stopped due to a fatal error: ')
            break
        except Exception:
            logger.exception('StashBox exception: ')


if __name__ == '__main__':
    run()
