
import logging
import os

from logging import StreamHandler
from logging.handlers import RotatingFileHandler

from stashbox.constants import LOG_DIR


class StashLogger(logging.Logger):
    """
    Class that sets up logging for StashBox.
    """

    def __init__(self, name: str):
        super().__init__(name)
        log_path = os.path.join(LOG_DIR, 'stashbox.log')
        os.makedirs(os.path.dirname(log_path), exist_ok=True)
        file_handler = RotatingFileHandler(
            filename=log_path,
            maxBytes=10 * 1024 ** 2,
            backupCount=10
        )
        formatter = logging.Formatter(
            fmt='{asctime}: [{levelname}] {message}',
            style='{'
        )
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logging.WARNING)
        self.addHandler(file_handler)

        stream_handler = StreamHandler()
        stream_handler.setFormatter(formatter)
        stream_handler.setLevel(logging.INFO)
        self.addHandler(stream_handler)

        self.setLevel(logging.DEBUG)


def setup_logger():
    """
    Designate StashLogger as the official logger.
    """
    logging.setLoggerClass(StashLogger)
