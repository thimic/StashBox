#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import logging
import os
import time

from concurrent.futures import ThreadPoolExecutor

from dropbox import Dropbox
from dropbox.files import (
    WriteMode,
    CreateFolderError,
    WriteError,
    WriteConflictError
)
from dropbox.exceptions import ApiError, BadInputError
from stashbox.constants import (
    DROPBOX_ACCESS_TOKEN,
    SOURCE,
    DESTINATION,
    INTERVAL
)
from stashbox.exceptions import FatalStashBoxError
from stashbox.utils import compute_dropbox_hash
from tqdm import tqdm

logger = logging.getLogger(__name__)


class StashBox(object):

    def __init__(self):
        """
        Class constructor for StashBox.
        """
        self._dbx = Dropbox(DROPBOX_ACCESS_TOKEN, timeout=90)
        self._upload_paths = []
        self._existing_dest_paths = {}

    @property
    def dbx(self):
        """
        Instantiated Dropbox API

        Returns:
            dropbox.Dropbox: Dropbox API object

        """
        return self._dbx

    @property
    def paths(self):
        """
        A complete list of files and directories to back up.

        Returns:
            list[str]: Paths to files and directories

        """
        return self._upload_paths

    @property
    def existing_dest_paths(self):
        """
        Dict of directories and files already on Dropbox. Key is the Dropbox
        path and the value the Dropbox entry object.

        Returns:
            dict[path: FileMetadata/FolderMetadata]: Dropbox entries by path

        """
        return self._existing_dest_paths

    def collect_local_paths(self):
        """
        Collect paths to be uploaded from the source directory.

        Returns:
            list[str]: List of paths

        """
        glob_root = os.path.join(SOURCE, '**', '*')
        self._upload_paths = glob.glob(glob_root, recursive=True)

    def collect_dropbox_paths(self):
        """
        Collects the total set of paths under the Dropbox destination root.

        Returns:
            dict[path: FileMetadata/FolderMetadata]: Dropbox entries by path

        """
        entries = []
        result = self.dbx.files_list_folder(DESTINATION, recursive=True)
        entries += result.entries
        while True:
            result = self.dbx.files_list_folder_continue(result.cursor)
            if not result.entries:
                break
            entries += result.entries
        self._existing_dest_paths = {e.path_display: e for e in entries}
        return self._existing_dest_paths

    def upload_path(self, path):
        """
        Attempts to add the given path to Dropbox. If the path is a directory,
        a mirror directory will be created on Dropbox if on doesn't already
        exist. If the path is a file, it will be uploaded unless a file of the
        same name and hash exist on Dropbox already.

        Args:
            path (str): Path to file/directory

        Returns:

        """
        # Calculates Dropbox file path
        dbx_path = path.replace(SOURCE, DESTINATION)

        # Creating directories
        if os.path.isdir(path):

            # Skipping existing directories
            if dbx_path in self.existing_dest_paths:
                return
            logger.info(f'Creating directory: {dbx_path}')
            try:
                self.dbx.files_get_metadata(dbx_path)
            except Exception:
                self.dbx.files_create_folder_v2(dbx_path)

        # Uploading files
        elif os.path.isfile(path):

            # Checking if file already exists on Dropbox
            if dbx_path in self.existing_dest_paths:

                # If file exists on Dropbox, compare hashes. If hashes are
                # identical, don't upload a new one.
                local_hash = compute_dropbox_hash(path)
                dbx_hash = self.existing_dest_paths[dbx_path].content_hash
                if local_hash == dbx_hash:
                    logger.debug(f'No changes, skipping file: {path}')
                    return
                logger.debug(f'Updating changed file: {path}')
            else:
                logger.debug(f'Uploading new file: {path}')

            # Uploading file in overwrite mode.
            with open(path, 'rb') as f:
                self.dbx.files_upload(
                    f.read(), dbx_path, mode=WriteMode.overwrite, mute=True
                )

    def start(self):
        """
        Performs the backup.
        """
        logger.info('Initiating backup...')

        # Create destination dir if it doesn't exist
        try:
            self._dbx.files_create_folder_v2(DESTINATION)
        except BadInputError:
            raise
        except ApiError as error:
            dbx_err = error.error.get_path()
            if not isinstance(dbx_err, (CreateFolderError, WriteError)):
                raise FatalStashBoxError from error
            if not isinstance(dbx_err.get_conflict(), WriteConflictError):
                raise FatalStashBoxError from error

        while True:

            # Compile a list of path to be uploaded
            self.collect_local_paths()

            # Compile a list of directories and files already on Dropbox
            self.collect_dropbox_paths()

            # Upload dirs first
            files = []
            for path in self.paths:
                if os.path.isdir(path):
                    self.upload_path(path)
                else:
                    files.append(path)

            # Upload files
            process_count = int(max(min(os.cpu_count() / 2, 4), 1))
            with ThreadPoolExecutor(process_count) as executor:
                kwargs = {
                    'total': len(files),
                    'unit': 'file',
                    'leave': True,
                    'ncols': 100,
                    'disable': None,
                }
                list(tqdm(executor.map(self.upload_path, files), **kwargs))

            executor.shutdown(wait=True)

            # Sleep
            logger.info(f'Backup complete. Sleeping for {INTERVAL} minutes.')
            time.sleep(INTERVAL * 60)

