
FROM python:3.8-alpine

ENV STASHBOX /usr/src/StashBox
WORKDIR ${STASHBOX}

COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY stashbox ./stashbox

ENV PYTHONPATH ${STASHBOX}

ENV SB_SOURCE "/data"

CMD python stashbox/run.py
